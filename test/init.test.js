import chai from "chai";
import chaiHttp from "chai-http";
import { describe, it } from "mocha";
import app from '../app.js';

// Utilisation de chaiHttp avec chai
chai.use(chaiHttp);
const should = chai.should();

describe("init", () => {
    it("test home page of server", (done) => {
        chai.request(app) // Utilisation de l'instance app directement
            .get("/")
            .end((err, res) => {
                should.not.exist(err);
                res.should.have.status(200);
                done();
            });
    });
});
