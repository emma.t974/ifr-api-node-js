import chai from "chai";
import chaiHttp from "chai-http";
import { describe, it, before } from "mocha";
import app from '../app.js';

// Utilisation de chaiHttp avec chai
chai.use(chaiHttp);
const expect = chai.expect;

describe("Task Operations", () => {
    let authToken = "";

    before(async () => {
        const loginResponse = await chai.request(app)
            .post("/api/login")
            .send({ email: process.env.TEST_USER, password: process.env.TEST_PASSWORD });

        authToken = loginResponse.body.token;
        expect(loginResponse).to.have.status(200);
    });

    it("should add a task successfully", async () => {
        const taskResponse = await chai.request(app)
            .post("/api/tasks")
            .set('Authorization', `Bearer ${authToken}`)
            .send({ name: "New Task", description: "Task description", completed: false });

        expect(taskResponse).to.have.status(201);
    });
});