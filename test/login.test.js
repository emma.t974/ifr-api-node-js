import chai from "chai";
import chaiHttp from "chai-http";
import { describe, it } from "mocha";
import app from '../app.js';

// Utilisation de chaiHttp avec chai
chai.use(chaiHttp);
const should = chai.should();

describe("login", () => {
    beforeEach(function (done) {
        done();
    });

    it("should fail to log in with incorrect credentials", (done) => {
        chai.request(app)
            .post("/api/login")
            .send({ email: "wrong@example.com", password: "wrongpassword" })
            .end((err, res) => {
                should.not.exist(err);
                res.should.have.status(401); // Assurez-vous que votre API renvoie bien 404 pour un échec de connexion, ce pourrait aussi être 401
                done();
            });
    });

    it("should log in successfully with correct credentials", (done) => {
        chai.request(app)
            .post("/api/login")
            .send({ email: process.env.TEST_USER, password: process.env.TEST_PASSWORD }) // Assurez-vous que ces variables d'environnement sont définies
            .end((err, res) => {
                should.not.exist(err);
                res.should.have.status(200);
                done();
            });
    });
});