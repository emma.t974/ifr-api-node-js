
# Api de liste de tâche + user
Cette api permet de récupérer une liste de tâche et ces listes d'utilisateurs. Il y a un système de middleware si vous souhaitez vous inspirez en terme de code.

## End point
### Tâches
|  HTTP| ROUTE | DESCRIPTION |
|--|--|--|
| GET | /api/tasks | Permet de récupères ensemble des tâches |
| GET | /api/tasks/:id | Permet de une tâche par rapport à son id |
| POST | /api/tasks | Permet de créer une nouvelle tâche |
| PUT | /api/tasks/:id | Permet de mettre à jour une tâche |
| DELETE | /api/tasks/:id | Permet de supprimer une tâche |

Structure de la données

    _id: ObjectID,
    name:  String,
    description:  String
    date:  Date
    completed:  Boolean
    assignedTo: ObjectID

### Utilisateurs

|  HTTP| ROUTE | DESCRIPTION |
|--|--|--|
| GET | /api/users | Permet de récupères ensemble des utilisateurs |
| GET | /api/users/:id | Permet de un utilisateur à son id |
| POST | /api/users | Permet de créer un nouveau utilisateur |
| PUT | /api/users/:id | Permet de mettre à jour un utilisateur |
| DELETE | /api/users/:id | Permet de supprimer un utilisateur |

Structure de la données

    _id: ObjectID,
    name:  String,
    email:  String
    password:  String
    date:  Date

### Auth

|  HTTP| ROUTE | DESCRIPTION |
|--|--|--|
| POST | /api/login | Permet de se connecter, renvoie un token |

### Upload

|  HTTP| ROUTE | DESCRIPTION |
|--|--|--|
| POST | /api/upload | Permet d'upload une image, en multipart/form |

### GraphQL

L'api propose également la possibilité d'utiliser du GraphQL, pour pouvoir utiliser du GraphQL rendez-vous sur 

    /graphql

## Test

Vous pouvez lancer des tests unitaires utilisant la commande

    npm run test

## Installation

Pour installer le projet il suffit de faire un clone du projet en lançant la commande :

    git clone  https://gitlab.com/emma.t974/ifr-api-node-js.git

Puis de vous rendre dans le projet et de lancer la commande :

    npm install

Il est nécessaire d'avoir MongoDB installer sur sa machine, vous pouvez télécharger ici :

https://www.mongodb.com/try/download/compass

## Lancer le projet

Pour pouvoir lancer le projet, il suffit de faire la commande :

    npm run start

## Documentation

Pour pouvoir accéder à la documentation, vous pouvez soit la générée avec la commande :

    npm run apidoc

Soit vous lancez le serveur, la documentation se génèra automatiquement à ce moment la.

Pour pouvoir accéder à la documentation, sur votre navigateur, rendez-vous sur le lien : http://localhost:3000/doc
