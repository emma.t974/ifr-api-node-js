import jwt from 'jsonwebtoken'

// Permet de vérifier si l'utilisateur est connecté ou non
export function authMiddleware(req, res, next) {
    // On récupère les entêtes de la requête (ici Bearer)
    const authHeader = req.headers['authorization']
    // On sépare le Bearer et la donnée
    const token = authHeader && authHeader.split(' ')[1]

    // Si le token est vide, on retourne qu'on n'est pas connecté
    if (token == null) {
        return res.status(401).json({ message: "No login" })
    }

    // On vérifie la validité du token, s'il est valide, on enregistre 
    // l'utilisateur et on continue le process
    jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
        if (err) {
            return res.sendStatus(403)
        }
        req.user = user
        next()
    });
}