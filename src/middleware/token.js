import jwt from 'jsonwebtoken'

// Permet de générée un token
export function generateToken(userId) {
    // Le Token est générée depuis une clé secret qu'on a configurer
    const secretKey = process.env.JWT_SECRET
    // On signe le token et on lui attribue une durée de vie
    const token = jwt.sign({ id: userId }, secretKey, { expiresIn: '1h' })
    return token
}