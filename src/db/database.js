import mongoose from 'mongoose';

// Permet de ce connecté à la base de donnée
export default class Database {
    constructor() {
        this.connect();
    }

    async connect() {
        await mongoose.connect(process.env.MONGO).catch(err => console.error(err));
        console.log("Pinged your deployment. You successfully connected to MongoDB!");
    }
}
