import Task from '../models/Task.js'
import { taskValidation } from '../validators/task.js'

// On récupère tous les tâches d'un utilisateur
export async function getTasks(req, res) {
    try {
        // On récupère le modèle tâche
        const taskModel = new Task()
        // On execute la requête pour récupérée tous les tâches d'un utilisateur connecté
        // const tasks = await taskModel.getAll({ assignedTo: req.user.id })
        const tasks = await taskModel.getAll()
        // On renvoie tous les tâches avec code http 200 
        return res.status(200).json(tasks)
    } catch (error) {
        // En cas de problème serveur
        return res.status(500).json({ message: error.message })
    }
}

// On récupère une tâche spécifique
export async function getTask(req, res) {
    try {
        // On récupère le modèle tâche
        const taskModel = new Task()
        // On execute la requête pour une tâche d'un utilisateur connecté
        const task = await taskModel.getById({ _id: req.params.id, assignedTo: req.user.id })
        // Si la tâche n'existe pas ou ne correspond pas à notre utilisateur
        if (!task) {
            return res.status(404).json({ message: `La tâche avec ID ${req.params.id} n'existe pas` })
        }
        // On renvoie la tâche en question avec un code HTTP de 200
        return res.status(200).json(task)
    } catch (error) {
        // En cas de problème serveur
        return res.status(500).json({ message: error.message })
    }
}

// Pour pouvoir ajouter une tâche à un utilisateur
export async function insertTask(req, res) {
    try {
        // On vérifie la validation des données
        const { error } = taskValidation.validate(req.body)
        // En cas de problème de données, on lui renvoie les erreurs de la donnée invalidité
        if (error) {
            return res.status(400).send(error.details[0].message);
        }
        // On récupère le modèle tâche
        const taskModel = new Task()
        // On ajoute à nos paramètres à qui la tâche sera attribuer
        req.body['assignedTo'] = req.user.id
        // On execute la requête pour ajouter une tâche d'un utilisateur connecté
        const newTask = await taskModel.insert(req.body)
        // On renvoie la tâche en question avec un code HTTP de 201
        return res.status(201).json(newTask)
    } catch (error) {
        // En cas de problème serveur
        return res.status(500).json({ message: error.message })
    }
}

// En cas de mise à jour d'une tâche
export async function updateTask(req, res) {
    try {
        // On récupère le modèle tâche
        const taskModel = new Task()
        // On execute la requête pour modifier d'une tâche d'un utilisateur connecté
        const updateTask = await taskModel.update({ _id: req.params.id }, req.body)
        // Si la tâche n'existe pas
        if (!updateTask) {
            return res.status(404).json({ message: `La tâche avec ID ${req.params.id} n'existe pas` })
        }
        // On renvoie un code HTTP de 204
        return res.status(204).json(updateTask)
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}

export async function removeTask(req, res) {
    try {
        // On récupère le modèle tâche
        const taskModel = new Task()
        // On execute la requête pour la suppresion d'une tâche d'un utilisateur connecté
        const deleteTask = await taskModel.remove({ _id: req.params.id })
        // Si la tâche n'existe pas
        if (!deleteTask) {
            return res.status(404).json({ message: `La tâche avec ID ${req.params.id} n'existe pas` })
        }
        // On renvoie un code HTTP de 204
        return res.status(204).json(deleteTask)
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}