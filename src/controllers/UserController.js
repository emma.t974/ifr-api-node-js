import User from '../models/User.js'
import { userValidation } from '../validators/user.js'
import argon2 from 'argon2'

// On récupère tous les utilisateurs
export async function getUsers(req, res) {
    try {
        // On récupère notre modèle
        const userModel = new User()
        // On execute la requête pour récupérée tous les utilisateurs
        const users = await userModel.getAll()
        // On renvoie tous les utilisateurs avec code http 200 
        return res.status(200).json(users)
    } catch (error) {
        // En cas de problème serveur
        return res.status(500).json({ message: error.message })
    }
}

// On récupère un utilisateur
export async function getUser(req, res) {
    try {
        // On récupère notre modèle
        const userModel = new User()
        // On execute la requête pour récupérée un utilisatuer depuis son ID
        const user = await userModel.getById({ _id: req.params.id })
        // Si l'utilisateur n'existe pas, on retourne un message d'erreur
        if (!user) {
            return res.status(404).json({ message: `L'utilisateur avec ID ${req.params.id} n'existe pas` })
        }
        // On retourne l'utilisateur
        return res.status(200).json(user)
    } catch (error) {
        // En cas de problème serveur
        return res.status(500).json({ message: error.message })
    }
}

// On ajoute un nouveau utilisateur
export async function insertUser(req, res) {
    try {
        // On vérifie la validation des données
        const { error } = userValidation.validate(req.body)
        // En cas de problème de données, on lui renvoie les erreurs de la donnée invalidité
        if (error) {
            return res.status(400).send(error.details[0].message);
        }
        // On récupère notre mot de passe et on le encrypte
        req.body['password'] = await argon2.hash(req.body['password'])
        // On récupère notre modèle
        const userModel = new User()
        // On execute la requête pour ajouter un nouveau utilisateur
        const newUser = await userModel.insert(req.body)
        // On renvoie le nouveau utilisateur avec un code HTTP de 201
        return res.status(201).json(newUser)
    } catch (error) {
        // En cas de problème serveur
        return res.status(500).json({ message: error.message })
    }
}

// Modification d'un utilisateur
export async function updateUser(req, res) {
    try {
        // On récupère notre modèle
        const userModel = new User()
        // On vérifie s'il y a nouveau de mot passe et l'encrypte
        if (req.body['password']) {
            req.body['password'] = await argon2.hash(req.body['password'])
        }
        // On execute la requête pour modifier l'utilisateur
        const updateUser = await userModel.update({ _id: req.params.id }, req.body)
        // En cas si l'utilisateur n'existe pas
        if (!updateUser) {
            return res.status(404).json({ message: `L'utilisateur avec ID ${req.params.id} n'existe pas` })
        }
        // On renvoie un code HTTP de 204
        return res.status(204).json(updateUser)
    } catch (error) {
        // En cas de problème serveur
        return res.status(500).json({ message: error.message })
    }
}
// Suppression d'un utilisateur
export async function removeUser(req, res) {
    try {
        // On récupère notre modèle
        const userModel = new User()
        // On execute la requête pour supprimer un utilisateur depuis son ID
        const deleteUser = await userModel.remove({ _id: req.params.id })
        // En cas si l'utilisateur n'existe pas
        if (!deleteUser) {
            return res.status(404).json({ message: `La tâche avec ID ${req.params.id} n'existe pas` })
        }
        // On renvoie un code HTTP de 204
        return res.status(204).json(deleteUser)
    } catch (error) {
        // En cas de problème serveur
        return res.status(500).json({ message: error.message })
    }
}