import multer from "multer"
import path from "path"

// On définie l'emplacement ou nous allons stocké les données et comment on va renommé notre fichier
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'tmp/uploads/')
    },
    filename: (req, file, cb) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        const extension = path.extname(file.originalname)
        const filename = `${file.fieldname}-${uniqueSuffix}${extension}`
        cb(null, filename)
    }
})

// Filtre des données, accepte que des images en format jpeg & png
const fileFilter = (req, file, cb) => {
    if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
        cb(null, true)
    } else {
        cb(new Error('Seulement des images en format .jpeg ou png'), false)
    }
}

// On utilise multer pour pouvoir upload nos images
const upload = multer({ storage: storage, fileFilter: fileFilter })

// On créer notre fonction pour pouvoir upload un fichier à la fois
export function uploadFile(req, res, next) {
    upload.single('image')(req, res, (err) => {
        if (err) {
            return next(err)
        }
    })

    // const fileUrl = `${req.protocol}://${req.hostname}:${process.env.PORT}/uploads/${req.file.filename}`

    return res.json({
        message: "Image upload réussite",
        // url: fileUrl
    })
}
