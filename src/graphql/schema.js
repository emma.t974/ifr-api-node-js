import { buildSchema } from "graphql";

// Défini le type de donnée que l'on souhaite récupérée/afficher ainsi que les données attendu
export const schema = buildSchema(`
    type Task {
        _id: ID!
        name: String!
        description: String!
        completed: Boolean!
        assignedTo: String!
    }

    type User {
        _id: ID!
        name: String!,
        email: String!
    }


    type Query {
        tasks:[Task!],
        users:[User!]
    }

    schema {
        query: Query
    }
`)