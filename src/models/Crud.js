import mongoose from 'mongoose';
import Database from '../db/database.js';

export default class Crud extends Database {
    constructor(modelIndex, schema) {
        // Hérite pour pouvoir accéder à la base de donnée
        super()
        // Récupère le schéma depuis mongoose
        const { Schema } = mongoose

        // En cas de conflit de modèle existant
        if (mongoose.models[modelIndex]) {
            delete mongoose.models[modelIndex]
        }

        // Créer le schéma de nos données
        const schemaModel = new Schema(schema)

        // Permet de créer un modèle
        this.model = mongoose.model(modelIndex, schemaModel)
    }

    // Rechercher une entité par id
    async getById(id) {
        return await this.model.findById(id).exec()
    }

    // Affiche tous les entités
    async getAll() {
        return await this.model.find().exec()
    }

    // Ajoute d'une nouvelle entité
    async insert(data) {
        const query = new this.model(data)

        return await query.save()
    }

    // Mets à jour une entité
    async update(id, data) {
        return await this.model.findByIdAndUpdate(id, data, { new: true })
    }

    // Supprime une entité
    async remove(id) {
        return await this.model.findByIdAndRemove(id).exec()
    }
}