import Crud from './Crud.js'
import mongoose from 'mongoose'

export default class Task extends Crud {
    constructor() {
        // Créer notre modèle et son schéma spécifique
        super('task', {
            name: String,
            description: String,
            date: { type: Date, default: Date.now },
            completed: Boolean,
            assignedTo: mongoose.Schema.Types.ObjectId
        })
    }

    // Affiche tous les données en excluant le assignedTo
    async getAll(userId) {
        return await this.model.find(userId).select('-assignedTo').exec()
    }

    // Récupère une tâche en excluant le assignedTo
    async getById(id) {
        return await this.model.find(id).select('-assignedTo').exec()
    }
}