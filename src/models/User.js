import Crud from './Crud.js'

export default class User extends Crud {
    constructor() {
        // Créer notre modèle et son schéma spécifique
        super('user', {
            name: String,
            email: String,
            password: String,
            date: { type: Date, default: Date.now },
        })
    }

    // Affiche tous les données en récupérant uniquement le nom & l'email
    async getAll() {
        return await this.model.find().select('name email').exec()
    }

    // Affiche tous les données d'un utilisateur en excluant son mot de passe
    async getById(id) {
        return await this.model.findById(id).select('-password').exec()
    }

    // Récupère un utilisateur depuis son adresse email
    async getByEmail(data) {
        return await this.model.findOne({ email: data['email'] }).exec()
    }
}