import express from 'express'
import { authMiddleware } from './middleware/auth.js';
import { getTask, getTasks, insertTask, removeTask, updateTask } from './controllers/TaskController.js'
import { getUser, getUsers, insertUser, removeUser, updateUser } from './controllers/UserController.js'
import { login } from './controllers/AuthController.js'
import { uploadFile } from './controllers/UploadController.js'

const router = express.Router();

router.get('/', (req, res) => {
    res.send({
        'message': 'Hello'
    })
})

/**
 * @api {get} /api/tasks Récupère tous les tâches de l'utilisateur connecté
 * @apiName GetTasks
 * @apiGroup Tâches
 *
 */
router.get('/api/tasks', getTasks)
/**
 * @api {post} /api/tasks Créer une nouvelle tâche
 * @apiName PostTask
 * @apiGroup Tâches
 * @apiVersion 1.0.0
 *
 * @apiSuccess {String} name  Le nom de la tâche
 * @apiSuccess {String} description Description plus détaillez de la tâche
 * @apiSuccess {Boolean} completed État actuelle de la tâche (si elle a été compléter ou non)
 */
router.post('/api/tasks', authMiddleware, insertTask)
/**
 * @api {put} /api/tasks/:id Modifier une tâche
 * @apiName PutTask
 * @apiGroup Tâches
 * @apiVersion 1.0.0
 * 
 * @apiParam {ObjectId} id Identifiant unique de la tâche
 * 
 * @apiSuccess {String} name  Le nom de la tâche
 * @apiSuccess {String} description Description plus détaillez de la tâche
 * @apiSuccess {Boolean} completed État actuelle de la tâche (si elle a été compléter ou non)
 */
router.put('/api/tasks/:id', authMiddleware, updateTask)
/**
 * @api {delete} /api/tasks/:id Supprimer une tâche
 * @apiName DeleteTask
 * @apiGroup Tâches
 * @apiVersion 1.0.0
 *
 * @apiParam {ObjectId} id Identifiant unique de la tâche
 * 
 */
router.delete('/api/tasks/:id', authMiddleware, removeTask)
/**
 * @api {get} /api/tasks/:id Récupère une tâche détaillez
 * @apiName GetTask
 * @apiGroup Tâches
 * @apiVersion 1.0.0
 *
 * @apiParam {ObjectId} id Identifiant unique de la tâche
 * 
 */
router.get('/api/tasks/:id', authMiddleware, getTask)

/**
 * @api {get} /api/users Récupère tous les utilisateurs
 * @apiName GetUsers
 * @apiGroup Utilisateurs
 * @apiVersion 1.0.0
 *
 */
router.get('/api/users', getUsers)

/**
 * @api {post} /api/users Créer un nouveau utilisateur
 * @apiName PostUser
 * @apiGroup Utilisateurs
 * @apiVersion 1.0.0
 *
 *
 * @apiSuccess {String} name  Le nom & prénom de l'utilisateur
 * @apiSuccess {String} email Adresse email de l'utilisateur
 * @apiSuccess {String} password Mot de passe de l'utilisateur
 */
router.post('/api/users', insertUser)

/**
 * @api {put} /api/users/:id Mettre à jour un utilisateur 
 * @apiName PutUser
 * @apiGroup Utilisateurs
 * @apiVersion 1.0.0
 *
 * @apiParam {ObjectId} id Identifiant unique de l'utilisateur
 *
 * @apiSuccess {String} name  Le nom & prénom de l'utilisateur
 * @apiSuccess {String} email Adresse email de l'utilisateur
 * @apiSuccess {String} password Mot de passe de l'utilisateur
 */
router.put('/api/users/:id', authMiddleware, updateUser)

/**
 * @api {delete} /api/users/:id Supprime un utilisateur
 * @apiName DeleteUser
 * @apiGroup Utilisateurs
 * @apiVersion 1.0.0
 *
 * @apiParam {ObjectId} id Identifiant unique de l'utilisateur
 *
 */
router.delete('/api/users/:id', authMiddleware, removeUser)

/**
 * @api {get} /api/users/:id Récupère un utilisateur
 * @apiName GetUser
 * @apiGroup Utilisateurs
 * @apiVersion 1.0.0
 *
 * @apiParam {ObjectId} id Identifiant unique de l'utilisateur
 *
 */
router.get('/api/users/:id', getUser)

/**
 * @api {post} /api/login Permet la connexion d'un utilisateur
 * @apiName Login
 * @apiGroup Authentification
 * @apiVersion 1.0.0
 *
 * @apiSuccess {String} email Adresse email de l'utilisateur
 * @apiSuccess {String} password Mot de passe de l'utilisateur
 *
 */
router.post('/api/login', login)

/**
 * @api {post} /api/upload Permet l'envoie d'un fichier
 * @apiName PostUpload
 * @apiGroup Upload
 * @apiVersion 1.0.0
 *
 * @apiSuccess {File} file Adresse email de l'utilisateur
 *
 */
router.post('/api/upload', uploadFile)

export default router