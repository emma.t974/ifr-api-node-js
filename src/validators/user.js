import Joi from 'joi';

/**
 * 
 * Vérifie le type donnée et si celui-ci est obligatoire ou pas
 * nom : String, obligatoire
 * email: String, obligatoire
 * password: String, obligatoire
 * date: Date
 * state: Boolean
 * 
 *  */
export const userValidation = Joi.object({
    name: Joi.string().required(),
    email: Joi.string().required(),
    password: Joi.string().required(),
    date: Joi.date(),
    state: Joi.boolean()
});