import Joi from 'joi';

/**
 * 
 * Vérifie le type donnée et si celui-ci est obligatoire ou pas
 * nom : String, obligatoire
 * description: String, obligatoire
 * date: Date
 * completed: Boolean
 * 
 *  */
export const taskValidation = Joi.object({
    name: Joi.string().required(),
    description: Joi.string().required(),
    date: Joi.date(),
    completed: Joi.boolean()
});