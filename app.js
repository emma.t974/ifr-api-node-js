import process from "process";
import express from 'express'
import bodyParser from 'body-parser'
import apicache from 'apicache'
import routes from './src/routes.js'
import dotenv from 'dotenv'
import { graphqlHTTP } from "express-graphql"
import { schema } from './src/graphql/schema.js'
import { resolver } from './src/graphql/resolves.js'
import path from 'path'
import { fileURLToPath } from 'url';
import cors from "cors"
import { RateLimiterRedis } from 'rate-limiter-flexible';
import redis from "redis";

// Configuration .env
dotenv.config()

// Configuration express
const app = express()
const port = 3000

// Configuration apidoc
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
app.use("/doc", express.static(path.join(__dirname, '/apidoc/')))

app.use(express.static(path.join(__dirname, '/public/')));

// cors
app.use(cors())

// Configure cache, cache uniquement les code statut = 200
let cache = apicache.middleware
app.use(cache('5 minutes', ((req, res) => res.statusCode === 200)))

// Configuration header
app.use((req, res, next) => {
    if (
        req.method === "POST"
        || req.method === "PUT"
        || req.method === "DELETE"
    ) {
        apicache.clear();
    }
    res.type('application/json')
    next()
});


// Configure les params
app.use(bodyParser.json())
app.use(express.urlencoded({ extended: true }))

// Configure route
app.use(routes)

// PARTIR GRAPHQL
app.use(
    "/graphql",
    graphqlHTTP({
        schema: schema,
        rootValue: resolver,
        graphiql: true,
    })
)

if (process.env.APP_DEV == "production") {
    // Configuration redis
    const redisClient = redis.createClient({
    });

    // Configuration rateLimiter
    const rateLimiter = new RateLimiterRedis({
        storeClient: redisClient,
        points: 5,
        duration: 1
    });

    app.use((req, res, next) => {
        rateLimiter.consume(req.ip).then(() => {
            next();
        }).catch(() => {
            res.status(429).send('Too Many Request')
        });
    });

}



// Configure server
app.listen(port, () => {
    console.log(`listen to ${port}`)
});

export default app;